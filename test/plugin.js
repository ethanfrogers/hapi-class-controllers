const Code = require('code');   // assertion library
const Lab = require('lab');
const lab = exports.lab = Lab.script();

const Wreck = require('wreck');
const Hapi = require('hapi');

let TestServer = new Hapi.Server({debug: false});
TestServer.connection();

lab.experiment("Test that server routes to the proper method", () => {
	lab.before(() => {
		return TestServer.register({
			register: require('../'),
			options: {
				controllers: require("./controllers")
			}
		}).then(() => {
			return TestServer.start();
		});
	});

	lab.test("returns 'I returned from TestController.test.'", (done) => {
		const connection = TestServer.info;
		Wreck.get(`${connection.uri}/test`,{json: true}, (err, response, payload) => {
			if(err) return done(err);
			Code.expect(payload.toString()).to.equal("I returned from TestController.test.");
			done();	
		});
		
	});

	lab.test("routes that throw errors propagate properly.'", (done) => {
		const connection = TestServer.info;
		Wreck.get(`${connection.uri}/bad-route`,{json: true}, (err, response, payload) => {
			if(err) return done(err);
			Code.expect(response.statusCode).to.equal(500);
			Code.expect(response.statusMessage).to.equal("Internal Server Error");
			done();
		});
		
	});

	lab.after(() => {
		return TestServer.stop();
	});
});

lab.experiment("errors should propogate from the plugin", () => {
	lab.test("should get an error", () => {
		let badServer = new Hapi.Server();
		badServer.connection();
		return badServer.register({
			register: require('../'),
			options: {
				controllers: require("./controllers/PoorlyConfiguredController")
			}
		}).catch((err) => {
			Code.expect(err.message).to.equal("Unknown authentication strategy admin in /test");
		});
	});
});