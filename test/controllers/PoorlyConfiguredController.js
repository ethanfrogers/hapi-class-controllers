'use strict'

class PoorlyConfiguredController {

	static get routes(){
		return [
			{
				method: "GET",
				path: "/bad",
				handler: "test",
				config:{
					auth:"admin"
				}
			}
		];
	}

	test(request, reply){
		return reply("I returned from TestController.test.");
	}

}

module.exports = PoorlyConfiguredController;