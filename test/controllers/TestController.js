'use strict'

class TestController {

	static get routes(){
		return [
			{
				method: "GET",
				path: "/test",
				handler: "test"
			},
			{
				method: "GET",
				path: "/bad-route",
				handler:"badRoute"
			}
		];
	}

	test(request, reply){
		return reply("I returned from TestController.test.");
	}

	badRoute(request, reply){
		throw new Error("Bad Route");
		return reply({});
	}

}

module.exports = TestController;