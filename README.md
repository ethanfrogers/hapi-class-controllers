# hapi-class-controllers

Write controllers as ES6 classes and use them as routes in [Hapi](http://hapijs.com)!


## Installation

```
$ npm install --save hapi-class-controllers
```

## Usage

Create an ES6 class to use as a controller. (Must have a static `routes` getter)

```
//controllers/AppController.js
'use strict'

exports.AppController = class AppController {
	static get routes(){
		return [
			{
				"method": "GET",
				path: "/",
				handler: "index"
			}
		];
	}

	index(request, reply){
		return reply("Hello world!");
	}
}

```

Then, register the plugin, telling it where to find your controllers.

```
//server.js
'use strict'

const Hapi = require("hapi");

const Server = new Hapi.Server();

Server.connection();

Server.register({
	register: require("hapi-class-controllers"),
	options: {
		controllers: require("./controllers/AppController")
	}
})
.then((err) => {
	if(err) throw err;
});

Server.start()
	.then((err) => {
		if(err) throw err;
	})

```


## Options
* `options.controllers`: Either a string or module defining your controllers. If option is a string, `hapi-class-controllers` will attempt to `require` your controllers based on the path provided. You `controllers` module should export an object where each key is the controller name and the value is the class. A new instance of each controller will be created at registration.