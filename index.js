'use strict';

exports.register = (server, options, next) => {
	if(!options.controllers){
		return next("hapi-class-controllers requires either a path to controllers module or a controllers module.");
	}

	if(typeof options.controllers === 'string'){
		options.controllers = require(options.controllers);
	}

	const controllers = options.controllers;

	for(let controllerName in controllers){
		let controller = controllers[controllerName];
		if(!controller.routes){
			return next("hapi-class-controllers requires a static routes property on each controller.");
		}
		let instance = new (controller)({server: server});
		let routes = controller.routes;
		routes.forEach((route) => {
			let methodName = route.handler;
			let handler = `${controllerName}.${methodName}`;
			route.handler = instance[methodName];
			if(!route.config){
				route.config = {bind: instance};
			}
			try{
				server.route(route);
			}
			catch(err){
				return next(err);
			}
			
		});
	}
	next();
};

exports.register.attributes = {
	pkg: require('./package.json')
};